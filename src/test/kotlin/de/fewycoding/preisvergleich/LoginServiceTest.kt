package de.fewycoding.preisvergleich

import de.fewycoding.preisvergleich.entity.UnternehmenEntity
import de.fewycoding.preisvergleich.exception.LoginException
import de.fewycoding.preisvergleich.model.LoginDTO
import de.fewycoding.preisvergleich.repository.UnternehmensRepository
import de.fewycoding.preisvergleich.service.LoginService
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.assertThrows
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner

@SpringBootTest
@RunWith(SpringRunner::class)
class LoginServiceTest {

    @Autowired
    lateinit var loginService: LoginService
    @Autowired
    lateinit var unternehmensRepository: UnternehmensRepository

    private var currentID: Int = 0

    @Before
    fun initTestData() {
        val unternehmen = UnternehmenEntity(5, "HalloUnternehmen", "HalloAdresse", "60000",
                "HalloOrt", "HalloWelt", "HalloWelt123".hashCode(), null)
        currentID = unternehmensRepository.save(unternehmen).id!!

    }

    @After
    fun deleteTestDataKontaktanfragen() {
        unternehmensRepository.deleteById(currentID)
    }

    @Test
    fun loginServiceTestLoginErfolgreich(){
        val loginDTO = LoginDTO("HalloWelt","HalloWelt123")

        val response = loginService.login(loginDTO)

        assertEquals(HttpStatus.OK, response.statusCode)

    }

    @Test
    fun loginServiceTestLoginNichtErfolgreich(){
        val loginDTO = LoginDTO("HalloWelt","HalloWelt12345")

        val expectedException = assertThrows<LoginException> {
            loginService.login(loginDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Der Benutzername oder das Passwort stimmen nicht", expectedException.message)
    }
}