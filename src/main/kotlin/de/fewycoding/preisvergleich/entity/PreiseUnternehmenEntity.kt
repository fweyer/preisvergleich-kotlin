package de.fewycoding.preisvergleich.entity

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "preiseingabeUnternehmen", catalog = "")
data class PreiseUnternehmenEntity(

        @Column(name = "id")
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Int? = null,

        @Column(name = "Preis2700Liter", columnDefinition = "DECIMAL(7,2)")
        @Basic
        var preis2700Liter: Double,

        @Column(name = "Preis4850Liter", columnDefinition = "DECIMAL(7,2)")
        @Basic
        var preis4850Liter: Double,

        @Column(name = "Preis6400Liter", columnDefinition = "DECIMAL(7,2)")
        @Basic
        var preis6400Liter: Double,

        @Column(name = "Datum")
        @Basic
        var datum: LocalDate,

        @Column(name = "PLZ")
        @Basic
        var plz: String,

        @ManyToOne()
        @JoinColumn(name = "unternehmen_id", referencedColumnName = "id")
        //@JsonManagedReference
        var unternehmen: UnternehmenEntity? = null
)


