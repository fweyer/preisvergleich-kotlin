package de.fewycoding.preisvergleich.controller

import de.fewycoding.preisvergleich.model.KontaktanfrageDTO
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody

interface Kontaktanfrage {

    fun konstaktAnfrageSpeichern(@RequestBody kontaktAnfrageDTO: KontaktanfrageDTO?): ResponseEntity<String>

}