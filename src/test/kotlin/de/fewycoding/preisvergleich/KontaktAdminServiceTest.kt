package de.fewycoding.preisvergleich

import de.fewycoding.preisvergleich.entity.KontaktanfrageEntity
import de.fewycoding.preisvergleich.exception.KontaktanfrageException
import de.fewycoding.preisvergleich.model.KontaktanfrageDTO
import de.fewycoding.preisvergleich.repository.KontaktanfrageRepository
import de.fewycoding.preisvergleich.service.KontaktAdminService
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.assertThrows
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner

@SpringBootTest
@RunWith(SpringRunner::class)
class KontaktAdminServiceTest {

    @Autowired
    lateinit var kontaktAdminService: KontaktAdminService
    @Autowired
    lateinit var kontaktanfrageRepository: KontaktanfrageRepository

    @Before
    fun initTestData() {
        val kontaktanfrageOne = KontaktanfrageEntity(null, "TestNamen", "TestAdresse", "50000",
                "TestOrt", "TestPerson", "TestMail", "TestBetreff", "Testnachricht")
        val kontaktanfrageTwo = KontaktanfrageEntity(null, "MusterNamen", "MusterAdresse", "60000",
                "MusterOrt", "MusterPerson", "MusterMail", "MusterBetreff", "Musternachricht")
        val kontaktanfrageThree = KontaktanfrageEntity(null, "CheckNamen", "CheckAdresse", "70000",
                "CheckOrt", "CheckPerson", "CheckMail", "CheckBetreff", "Checknachricht")

        kontaktanfrageRepository.save(kontaktanfrageOne)
        kontaktanfrageRepository.save(kontaktanfrageTwo)
        kontaktanfrageRepository.save(kontaktanfrageThree)
    }

    @After
    fun deleteTestDataKontaktanfragen() {
        kontaktanfrageRepository.deleteAll()
    }

    @Test
    fun kontaktAdminServiceTest_ladeAlleKontakanfragenOK(){
        val response = kontaktAdminService.ladeKontakte()

        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(3, response.body?.size)
        assertEquals("MusterAdresse", response.body?.get(1)?.firmenadresse)
    }

    @Test
    fun kontaktAdminServiceTest_fuegeKontakanfrageEinOK(){
        val kontaktanfrageDTO = KontaktanfrageDTO(null,"BeispielGMBH","Beispielstrasse","53120",
                "Beispielhausen","Beispielmann","Beispielmail","Beispielbetreff",
                "Beispielnachrict",4)

        val response = kontaktAdminService.fuegeKontakteHinzu(kontaktanfrageDTO)

        assertEquals(HttpStatus.OK,response.statusCode)
    }

    @Test
    fun kontaktAdminServiceTest_fuegeKontakanfrageEinParamenterFehltException(){
        val kontaktanfrageDTO = KontaktanfrageDTO(null,"BeispielGMBH","Beispielstrasse",null,
                "Beispielhausen","Beispielmann","Beispielmail","Beispielbetreff",
                "Beispielnachrict",4)

        val expectedException = assertThrows<KontaktanfrageException> {
            kontaktAdminService.fuegeKontakteHinzu(kontaktanfrageDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Die eingegebenen Daten sind unvollständig!", expectedException.message)
    }

    @Test
    fun kontaktAdminServiceTest_andereKontaktAnfrageEinOK(){
        val listKontakt = kontaktanfrageRepository.findAll()
        val letztesElement = listKontakt.size - 1
        val kontaktanfrageDTO = KontaktanfrageDTO(listKontakt[letztesElement].id,"BeispielGMBH","Beispielstrasse","53120",
                "Beispielhausen","Beispielmann","Beispielmail","Beispielbetreff",
                "Beispielnachrict",4)

        val response = kontaktAdminService.aendereKontakte(kontaktanfrageDTO)

        assertEquals(HttpStatus.OK,response.statusCode)
    }

    @Test
    fun kontaktAdminServiceTest_andereKontaktAnfrageEinExceptionKeineID(){
        val kontaktanfrageDTO = KontaktanfrageDTO(null,"BeispielGMBH","Beispielstrasse",null,
                "Beispielhausen","Beispielmann","Beispielmail","Beispielbetreff",
                "Beispielnachrict",4)

        val expectedException = assertThrows<KontaktanfrageException> {
            kontaktAdminService.aendereKontakte(kontaktanfrageDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Diese Kontaktdaten können nicht aktualisiert werden", expectedException.message)
    }

    @Test
    fun kontaktAdminServiceTest_loescheKontaktAnfrageOK(){
        val listeKontakte = kontaktanfrageRepository.findAll()
        val kontaktanfrageDTO = KontaktanfrageDTO(listeKontakte[2].id,"CheckNamen", "CheckAdresse", "70000",
                "CheckOrt", "CheckPerson", "CheckMail", "CheckBetreff", "Checknachricht",3)

        val response = kontaktAdminService.loescheKontakte(kontaktanfrageDTO)

        assertEquals(HttpStatus.OK,response.statusCode)
        assertEquals(2,kontaktanfrageRepository.findAll().size)
    }

    @Test
    fun kontaktAdminServiceTest_loescheKontaktAnfrageExceptionKeineID(){
        val kontaktanfrageDTO = KontaktanfrageDTO(null,"CheckNamen", "CheckAdresse", "70000",
                "CheckOrt", "CheckPerson", "CheckMail", "CheckBetreff", "Checknachricht",3)

        val expectedException = assertThrows<KontaktanfrageException> {
            kontaktAdminService.loescheKontakte(kontaktanfrageDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Diese Kontaktdaten können nicht gelöscht werden", expectedException.message)
    }
}