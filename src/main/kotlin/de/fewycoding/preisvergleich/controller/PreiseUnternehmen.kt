package de.fewycoding.preisvergleich.controller

import PreisAnzeigeDTO
import de.fewycoding.preisvergleich.model.AnfrageDTO
import de.fewycoding.preisvergleich.model.PreiseingabeUnternehmenDTO
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody

interface PreiseUnternehmen {

    fun anfragePreise(@RequestBody anfrageDTO: AnfrageDTO?):  ResponseEntity<List<PreisAnzeigeDTO>>

    fun preiseingabeLogin(@RequestBody preiseingabeUnternehmenDTO: PreiseingabeUnternehmenDTO?): ResponseEntity<String>
}