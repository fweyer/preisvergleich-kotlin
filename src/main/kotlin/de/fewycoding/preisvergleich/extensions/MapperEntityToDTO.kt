package de.fewycoding.preisvergleich.extensions

import de.fewycoding.preisvergleich.entity.KontaktanfrageEntity
import de.fewycoding.preisvergleich.entity.PreiseUnternehmenEntity
import de.fewycoding.preisvergleich.entity.UnternehmenEntity
import de.fewycoding.preisvergleich.model.KontaktanfrageDTO
import de.fewycoding.preisvergleich.model.PreisDTO
import de.fewycoding.preisvergleich.model.PreiseingabeUnternehmenDTO
import de.fewycoding.preisvergleich.model.UnternehmenDTO
import java.time.LocalDate

fun KontaktanfrageDTO.toKontaktAnfrageEntity() = KontaktanfrageEntity(
        firmennamen = firmennamen,
        firmenadresse = firmenadresse,
        plz = plz,
        ort = ort,
        kontaktperson = kontaktperson,
        betreff = betreff,
        emailAdresse = eMail,
        nachricht = nachricht
)


fun PreisDTO.toPreisUnternehmenEntity() = PreiseUnternehmenEntity(
        preis2700Liter = preis2700liter!!.toDouble(),
        preis4850Liter = preis4850liter!!.toDouble(),
        preis6400Liter = preis6400liter!!.toDouble(),
        datum = LocalDate.now(),
        plz = plz.toString(),
        unternehmen = UnternehmenEntity(Integer.valueOf(unternehmen))
)


fun UnternehmenDTO.toUnternehmenEntity() = UnternehmenEntity(
        id = null,
        name = name,
        adresse = adresse,
        plz = plz,
        ort = ort,
        benutzername = benutzername
)

fun PreiseingabeUnternehmenDTO.toPreisUnterhnehmenEntity() = PreiseUnternehmenEntity(
        id = null,
        datum = LocalDate.now(),
        preis2700Liter = preis2700Liter!!.toDouble(),
        preis4850Liter = preis4850Liter!!.toDouble(),
        preis6400Liter = preis6400Liter!!.toDouble(),
        plz = postleitzahl.toString()
)