package de.fewycoding.preisvergleich.service

import de.fewycoding.preisvergleich.extensions.toUnternehmenDTO
import de.fewycoding.preisvergleich.extensions.toUnternehmenEntity
import de.fewycoding.preisvergleich.model.UnternehmenDTO
import de.fewycoding.preisvergleich.operations.UnternehmensOperations
import de.fewycoding.preisvergleich.repository.UnternehmensRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.lang.RuntimeException

@Service
class UnternehmenAdminService {


    @Autowired
    lateinit var unternehmenRepository: UnternehmensRepository

    fun ladeUnternehmen(): ResponseEntity<List<UnternehmenDTO>> {
        val listeUnternehmenDTO = mutableListOf<UnternehmenDTO>()
        unternehmenRepository.findAll().forEach { listeUnternehmenDTO.add(it.toUnternehmenDTO()) }
        listeUnternehmenDTO.sortByDescending { it.ID }
        listeUnternehmenDTO.forEachIndexed { index, dto -> dto.position = index + 1 }
        return ResponseEntity(listeUnternehmenDTO,HttpStatus.OK)
    }

    fun fuegeUnternehmenHinzu(unternehmenDTO: UnternehmenDTO): ResponseEntity<Unit> {
        UnternehmensOperations.validiereUnternehmenDTO(unternehmenDTO)
        val unternehmenEntity = unternehmenDTO.toUnternehmenEntity()
        unternehmenEntity.id = unternehmenRepository.findAll().size + 1
        unternehmenRepository.save(unternehmenEntity)
        return ResponseEntity(HttpStatus.OK)
    }

    fun aendereUnternehmen(unternehmenDTO: UnternehmenDTO): ResponseEntity<Unit> {
        if (unternehmenDTO.ID != null) {
            UnternehmensOperations.validiereUnternehmenDTO(unternehmenDTO)
            val oltEntity = unternehmenRepository.getOne(unternehmenDTO.ID!!)
            oltEntity.benutzername = unternehmenDTO.benutzername
            oltEntity.adresse = unternehmenDTO.adresse
            oltEntity.ort = unternehmenDTO.ort
            oltEntity.plz = unternehmenDTO.plz
            oltEntity.name = unternehmenDTO.name
            unternehmenRepository.save(oltEntity)
            return ResponseEntity(HttpStatus.OK)
        }
        throw RuntimeException("Diese Daten konnten nicht aktuallisiert werden")
    }

    fun loescheUnternehmen(unternehmenDTO: UnternehmenDTO): ResponseEntity<Unit> {
        if (unternehmenDTO.ID != null) {
            unternehmenRepository.deleteById(unternehmenDTO.ID!!)
            return ResponseEntity(HttpStatus.OK)
        }
        throw RuntimeException("Diese Unternehmensdaten können nicht gelöscht werden")
    }
}