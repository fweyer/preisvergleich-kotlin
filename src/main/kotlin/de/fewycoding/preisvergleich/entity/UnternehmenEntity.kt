package de.fewycoding.preisvergleich.entity

import javax.persistence.*


@Entity
@Table(name = "unternehmen", catalog = "")
data class UnternehmenEntity(

        @Column(name = "id")
        @Id
        var id: Int? = 0,

        @Column(name = "Name")
        @Basic
        var name: String? = null,

        @Column(name = "Adresse")
        @Basic
        var adresse: String? = null,

        @Column(name = "PLZ")
        @Basic
        var plz: String? = null,

        @Column(name = "Ort")
        @Basic
        var ort: String? = null,

        @Column(name = "Benutzername")
        @Basic
        var benutzername: String? = null,

        @Column(name = "Passwort")
        @Basic
        var passwort: Int? = null,

        @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL], mappedBy = "unternehmen")
        var preise: List<PreiseUnternehmenEntity>? = null
)


