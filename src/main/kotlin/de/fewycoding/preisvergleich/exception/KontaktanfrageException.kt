package de.fewycoding.preisvergleich.exception


class KontaktanfrageException(message: String?) : PreisvergleichException(message)