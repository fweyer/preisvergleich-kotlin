package de.fewycoding.preisvergleich.operations

import de.fewycoding.preisvergleich.entity.UnternehmenEntity
import de.fewycoding.preisvergleich.model.LoginDTO


class LoginOperations {
    companion object {
        // Login Logik
        // Json Objekt vom Frontend (LoginName und Passwort) annehmen und in der Methode checken
        // Loginname und passwort mit Datenbank vergleichen (UnternehmenEntits) wenn beide richtig
        // dann return "erfolgreich" sonst "fehlgeschlagen"
        fun loginCheck(loginDTO: LoginDTO?, unternehmenListe: List<UnternehmenEntity>): String {
            val passwortHash: Int? = loginDTO?.loginPasswort?.hashCode()
            for (unternehmen in unternehmenListe) {
                loginDTO?.let {
                    if (unternehmen.benutzername.equals(it.loginName) && unternehmen.passwort == passwortHash) {
                        return "erfolgreich"
                    }
                }
            }
            println("Die eingegebenen Benutzerdaten stimmen nicht")
            return "fehlgeschlagen"
        }
    }
}