package de.fewycoding.preisvergleich.entity

import javax.persistence.*

@Entity
@Table(name = "kontaktanfrage", catalog = "")
data class KontaktanfrageEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Int? = 0,
        var firmennamen: String? = null,
        var firmenadresse: String? = null,
        var ort: String? = null,
        var plz: String? = null,
        var kontaktperson: String? = null,
        var emailAdresse: String? = null,
        var nachricht: String? = null,
        var betreff: String? = null
)