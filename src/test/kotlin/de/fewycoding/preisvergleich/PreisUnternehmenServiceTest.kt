package de.fewycoding.preisvergleich

import de.fewycoding.preisvergleich.entity.PreiseUnternehmenEntity
import de.fewycoding.preisvergleich.entity.UnternehmenEntity
import de.fewycoding.preisvergleich.exception.PreiseingabeException
import de.fewycoding.preisvergleich.model.AnfrageDTO
import de.fewycoding.preisvergleich.model.PreiseingabeUnternehmenDTO
import de.fewycoding.preisvergleich.repository.UnternehmensRepository
import de.fewycoding.preisvergleich.service.PreiseUnternehmenService
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.assertThrows
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import java.time.LocalDate

@SpringBootTest
@RunWith(SpringRunner::class)
class PreisUnternehmenServiceTest {

    @Autowired
    lateinit var preiseUnternehmenService: PreiseUnternehmenService
    @Autowired
    lateinit var unternehmensRepository: UnternehmensRepository

    @Before
    fun initTestData() {
        val unternehmenOne = UnternehmenEntity(1, "Meinflüssiggas", "Irgendwo 169", "26212", "Oldenburg", "admin", "admin".hashCode())
        val unternehmenTwo = UnternehmenEntity(2, "Flüssiggas456", "Musterstraße 70", "98092", "Erfurt", "login", "login".hashCode())
        val unternehmenThree = UnternehmenEntity(3, "Beispielunternehmen", "ABC str.90", "50000", "Köln", "bespiel", "beispiel".hashCode())

        val preiseMeinFluessiggas = mutableListOf(
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(1)),
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(1)),
                PreiseUnternehmenEntity(null,40.00, 37.00, 36.00, LocalDate.of(2020,2,26),"51570", UnternehmenEntity(1)),
                PreiseUnternehmenEntity(null,40.00, 37.00 , 36.00 , LocalDate.of(2020,2,26), "51570", UnternehmenEntity(1)),
                PreiseUnternehmenEntity(null, 42.00, 41.00, 39.00, LocalDate.of(2020,2,27), "52152", UnternehmenEntity(1))

        )
        val preiseFluessiggas456 = mutableListOf(
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(2)),
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(2)),
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,26), "52152", UnternehmenEntity(2)),
                PreiseUnternehmenEntity(null,40.50, 37.50 , 36.00, LocalDate.of(2020,2,26), "51570", UnternehmenEntity(2)),
                PreiseUnternehmenEntity(null,40.50 , 37.50 , 36.00, LocalDate.of(2020,2,27),"51570", UnternehmenEntity(2)),
                PreiseUnternehmenEntity(null,42.00 , 41.00, 39.00, LocalDate.of(2020,2,27), "52152", UnternehmenEntity(2))

        )
        val preiseBeispielunternehmen = mutableListOf(
                PreiseUnternehmenEntity(null,38.30, 37.30, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(3)),
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(3)),
                PreiseUnternehmenEntity(null,40.30, 37.30, 36.00, LocalDate.of(2020,2,26), "51570", UnternehmenEntity(3)),
                PreiseUnternehmenEntity(null,38.00 , 37.00, 36.00, LocalDate.of(2020,2,26), "52152", UnternehmenEntity(3)),
                PreiseUnternehmenEntity(null,40.30, 37.30, 36.00, LocalDate.of(2020,2,27), "51570", UnternehmenEntity(3)),
                PreiseUnternehmenEntity(null,42.00, 41.00, 39.00, LocalDate.of(2020,2,27), "52152", UnternehmenEntity(3))
        )

        unternehmenOne.preise = preiseMeinFluessiggas
        unternehmenTwo.preise = preiseFluessiggas456
        unternehmenThree.preise = preiseBeispielunternehmen


        unternehmensRepository.save(unternehmenOne)
        unternehmensRepository.save(unternehmenTwo)
        unternehmensRepository.save(unternehmenThree)
    }

    @After
    fun deleteTestDataPreise() {
        unternehmensRepository.deleteAll()
    }

    @Test
    fun preisUnternehmenService_anfragePreiseOK2700(){
        val anfrageDTO = AnfrageDTO("preis2700liter","51570")

        val response = preiseUnternehmenService.anfragePreise(anfrageDTO)

        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(3, response.body?.size)
    }

    @Test
    fun preisUnternehmenService_anfragePreiseOK4850(){
        val anfrageDTO = AnfrageDTO("preis4850liter","52152")

        val response = preiseUnternehmenService.anfragePreise(anfrageDTO)

        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(3, response.body?.size)
    }

    @Test
    fun preisUnternehmenService_anfragePreiseOK6400(){
        val anfrageDTO = AnfrageDTO("preis6400liter","51570")

        val response = preiseUnternehmenService.anfragePreise(anfrageDTO)

        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(3, response.body?.size)
    }

    @Test
    fun preisUnternehmenService_anfragePreiseExeptionKeinePreisGefunden(){
        val anfrageDTO = AnfrageDTO("preis2700liter","10000")

        val expectedException = assertThrows<PreiseingabeException> {
            preiseUnternehmenService.anfragePreise(anfrageDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Keine Preise verfügbar", expectedException.message)
    }


    @Test
    fun preisUnternehmenService_preiseingabeLoginOK(){
        val preiseingabeUnternehmenDTO = PreiseingabeUnternehmenDTO("39.00","38.00","37.00","52152","login")

        val response = preiseUnternehmenService.preiseingabeLogin(preiseingabeUnternehmenDTO)

        assertEquals(HttpStatus.OK, response.statusCode)
    }


    @Test
    fun preisUnternehmenService_preiseingabeLoginException(){
        val preiseingabeUnternehmenDTO = PreiseingabeUnternehmenDTO("39.00","38.00","37.00","52152","bla")

        val expectedException = assertThrows<PreiseingabeException> {
            preiseUnternehmenService.preiseingabeLogin(preiseingabeUnternehmenDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Der Benutzer exsistiert nicht", expectedException.message)
    }
}