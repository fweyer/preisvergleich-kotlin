package de.fewycoding.preisvergleich.exception

class PreiseingabeException(message: String?) : PreisvergleichException(message)