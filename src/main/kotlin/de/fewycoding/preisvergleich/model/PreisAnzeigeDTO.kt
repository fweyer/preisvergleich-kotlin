data class PreisAnzeigeDTO(
        var id: Int? = null,
        var name: String? = null,
        var adresse: String? = null,
        var plz: String? = null,
        var ort: String? = null,
        var preis: String? = null
)



