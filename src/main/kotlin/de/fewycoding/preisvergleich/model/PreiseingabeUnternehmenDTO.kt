package de.fewycoding.preisvergleich.model

data class PreiseingabeUnternehmenDTO(
        var preis2700Liter: String? = null,
        var preis4850Liter: String? = null,
        var preis6400Liter: String? = null,
        var postleitzahl: String? = null,
        var benutzername: String? = null
)