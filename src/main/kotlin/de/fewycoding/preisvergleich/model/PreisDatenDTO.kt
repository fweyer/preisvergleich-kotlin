package de.fewycoding.preisvergleich.model

import java.time.LocalDate

data class PreisDatenDTO (
        var id: Int? = null,
        var preis: Double? = null,
        var datum: LocalDate? = null,
        var plz: String? = null,
        var unternehmenID: Int? = null,
        var unternehmenName: String? = null,
        var unternehmenAdresse: String? = null,
        var unternehmenPlz: String? = null,
        var unternehmenOrt: String? = null
)