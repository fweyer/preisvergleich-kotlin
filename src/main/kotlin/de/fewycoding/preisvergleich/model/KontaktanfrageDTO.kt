package de.fewycoding.preisvergleich.model

data class KontaktanfrageDTO(
        var ID: Int?,
        var firmennamen: String?,
        var firmenadresse: String?,
        var plz: String?,
        var ort: String?,
        var kontaktperson: String?,
        var eMail: String?,
        var betreff: String?,
        var nachricht: String?,
        var position: Int?
)