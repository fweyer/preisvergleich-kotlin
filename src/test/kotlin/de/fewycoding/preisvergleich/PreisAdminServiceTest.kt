package de.fewycoding.preisvergleich

import de.fewycoding.preisvergleich.entity.PreiseUnternehmenEntity
import de.fewycoding.preisvergleich.entity.UnternehmenEntity
import de.fewycoding.preisvergleich.exception.PreiseingabeException
import de.fewycoding.preisvergleich.model.PreisDTO
import de.fewycoding.preisvergleich.repository.PreiseingabeUnternehmenRepository
import de.fewycoding.preisvergleich.repository.UnternehmensRepository
import de.fewycoding.preisvergleich.service.PreisAdminService
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.assertThrows
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import java.lang.RuntimeException
import java.time.LocalDate

@SpringBootTest
@RunWith(SpringRunner::class)
class PreisAdminServiceTest {

    @Autowired
    lateinit var preisAdminService: PreisAdminService
    @Autowired
    lateinit var preiseingabeUnternehmenRepository: PreiseingabeUnternehmenRepository
    @Autowired
    lateinit var unternehmensRepository: UnternehmensRepository

    @Before
    fun initTestData() {
        val unternehmenOne = UnternehmenEntity(1, "Meinflüssiggas", "Irgendwo 169", "26212", "Oldenburg", "admin", "admin".hashCode())
        val unternehmenTwo = UnternehmenEntity(2, "Flüssiggas456", "Musterstraße 70", "98092", "Erfurt", "login", "login".hashCode())
        val unternehmenThree = UnternehmenEntity(3, "Beispielunternehmen", "ABC str.90", "50000", "Köln", "bespiel", "beispiel".hashCode())

        val preiseMeinFluessiggas = mutableListOf(
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(1)),
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(1)),
                PreiseUnternehmenEntity(null,40.00, 37.00, 36.00, LocalDate.of(2020,2,26),"51570", UnternehmenEntity(1)),
                PreiseUnternehmenEntity(null,40.00, 37.00 , 36.00 , LocalDate.of(2020,2,26), "51570", UnternehmenEntity(1)),
                PreiseUnternehmenEntity(null, 42.00, 41.00, 39.00, LocalDate.of(2020,2,27), "52152", UnternehmenEntity(1))

        )
        val preiseFluessiggas456 = mutableListOf(
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(2)),
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(2)),
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,26), "52152", UnternehmenEntity(2)),
                PreiseUnternehmenEntity(null,40.50, 37.50 , 36.00, LocalDate.of(2020,2,26), "51570", UnternehmenEntity(2)),
                PreiseUnternehmenEntity(null,40.50 , 37.50 , 36.00, LocalDate.of(2020,2,27),"51570", UnternehmenEntity(2)),
                PreiseUnternehmenEntity(null,42.00 , 41.00, 39.00, LocalDate.of(2020,2,27), "52152", UnternehmenEntity(2))

        )
        val preiseBeispielunternehmen = mutableListOf(
                PreiseUnternehmenEntity(null,38.30, 37.30, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(3)),
                PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(3)),
                PreiseUnternehmenEntity(null,40.30, 37.30, 36.00, LocalDate.of(2020,2,26), "51570", UnternehmenEntity(3)),
                PreiseUnternehmenEntity(null,38.00 , 37.00, 36.00, LocalDate.of(2020,2,26), "52152", UnternehmenEntity(3)),
                PreiseUnternehmenEntity(null,40.30, 37.30, 36.00, LocalDate.of(2020,2,27), "51570", UnternehmenEntity(3)),
                PreiseUnternehmenEntity(null,42.00, 41.00, 39.00, LocalDate.of(2020,2,27), "52152", UnternehmenEntity(3))
        )

        unternehmenOne.preise = preiseMeinFluessiggas
        unternehmenTwo.preise = preiseFluessiggas456
        unternehmenThree.preise = preiseBeispielunternehmen


        unternehmensRepository.save(unternehmenOne)
        unternehmensRepository.save(unternehmenTwo)
        unternehmensRepository.save(unternehmenThree)
    }

    @After
    fun deleteTestDataPreise() {
        unternehmensRepository.deleteAll()
    }

    @Test
    fun preisAdminServiceTest_ladePreise(){
        val response = preisAdminService.ladePreise()

        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(17, response.body?.size)
    }

    @Test
    fun preisAdminServiceTest_fuegePreiseHinzuOK(){
        val preisDTO = PreisDTO(null,LocalDate.now().toString(),"39.00","38.00","37.00","51570","1",1)

        val response = preisAdminService.fuegePreiseHinzu(preisDTO)

        assertEquals(HttpStatus.OK, response.statusCode)
    }

    @Test
    fun preisAdminServiceTest_fuegePreiseHinzuDatumFehlt(){
        val preisDTO = PreisDTO(null,null,"39.00","38.00","37.00","51570","1",1)

        val expectedException = assertThrows<PreiseingabeException> {
            preisAdminService.fuegePreiseHinzu(preisDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Der Eingegebene Preis ist leer oder enthält unvollständige Werte", expectedException.message)
    }

    @Test
    fun preisAdminServiceTest_anderePreiseOK(){
        val erstesElement = preiseingabeUnternehmenRepository.findAll()[0]

        val preisDTO = PreisDTO(erstesElement.id,LocalDate.now().toString(),"39.00","38.00","37.00","51570","1",1)

        val response = preisAdminService.aenderePreise(preisDTO)

        assertEquals(HttpStatus.OK, response.statusCode)
    }

    @Test
    fun preisAdminServiceTest_anderePreisePreisFehlt(){
        val erstesElement = preiseingabeUnternehmenRepository.findAll()[0]

        val preisDTO = PreisDTO(erstesElement.id,LocalDate.now().toString(),"39.00",null,"37.00","51570","1",1)

        val expectedException = assertThrows<PreiseingabeException> {
            preisAdminService.aenderePreise(preisDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Der Eingegebene Preis ist leer oder enthält unvollständige Werte", expectedException.message)
    }

    @Test
    fun preisAdminServiceTest_loeschePreiseOK(){
        val erstesElement = preiseingabeUnternehmenRepository.findAll()[0]

        val preisDTO = PreisDTO(erstesElement.id,LocalDate.now().toString(),"39.00","38.00","37.00","51570","1",1)

        val response = preisAdminService.loeschePreise(preisDTO)

        assertEquals(HttpStatus.OK, response.statusCode)
    }

    fun preisAdminServiceTest_loeschePreiseException(){
        val preisDTO = PreisDTO(null,LocalDate.now().toString(),"39.00","38.00","37.00","51570","1",1)

        val expectedException = assertThrows<RuntimeException> {
            preisAdminService.loeschePreise(preisDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Unternehmensdaten können nicht gelöscht werden!", expectedException.message)
    }
}