package de.fewycoding.preisvergleich.model

data class LoginDTO(
        var loginName: String?,
        var loginPasswort: String?
)