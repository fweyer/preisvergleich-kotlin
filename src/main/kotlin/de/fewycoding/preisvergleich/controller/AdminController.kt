package de.fewycoding.preisvergleich.controller

import de.fewycoding.preisvergleich.model.KontaktanfrageDTO
import de.fewycoding.preisvergleich.model.PreisDTO
import de.fewycoding.preisvergleich.model.UnternehmenDTO
import de.fewycoding.preisvergleich.service.KontaktAdminService
import de.fewycoding.preisvergleich.service.PreisAdminService
import de.fewycoding.preisvergleich.service.UnternehmenAdminService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping("/admin")
class AdminController : Admin {

    @Autowired
    lateinit var preisAdminService: PreisAdminService

    @Autowired
    lateinit var kontaktAdminService: KontaktAdminService

    @Autowired
    lateinit var unternehmenAdminService: UnternehmenAdminService

    @GetMapping("/ladePreise")
    override fun ladePreise(): ResponseEntity<List<PreisDTO>> {
        return preisAdminService.ladePreise()
    }

    @PostMapping("/fuegePreiseHinzu")
    override fun fuegePreiseHinzu(preisDTO: PreisDTO):ResponseEntity<Unit> {
        return preisAdminService.fuegePreiseHinzu(preisDTO)
    }

    @PostMapping("/aenderePreise")
    override fun aenderePreise(preisDTO: PreisDTO): ResponseEntity<Unit> {
        return preisAdminService.aenderePreise(preisDTO)
    }

    @PostMapping("/loeschePreise")
    override fun loeschePreise(preisDTO: PreisDTO): ResponseEntity<Unit> {
        return preisAdminService.loeschePreise(preisDTO)
    }

    @GetMapping("/ladeKontakte")
    override fun ladeKontakte(): ResponseEntity<List<KontaktanfrageDTO>> {
        return kontaktAdminService.ladeKontakte()
    }

    @PostMapping("/fuegeKontakteHinzu")
    override fun fuegeKontakteHinzu(kontaktanfrageDTO: KontaktanfrageDTO): ResponseEntity<Unit>{
        return kontaktAdminService.fuegeKontakteHinzu(kontaktanfrageDTO)
    }

    @PostMapping("/aendereKontakte")
    override fun aendereKontakte(kontaktanfrageDTO: KontaktanfrageDTO): ResponseEntity<Unit> {
        return kontaktAdminService.aendereKontakte(kontaktanfrageDTO)
    }

    @PostMapping("/loescheKontakte")
    override fun loescheKontakte(kontaktanfrageDTO: KontaktanfrageDTO): ResponseEntity<Unit> {
        return kontaktAdminService.loescheKontakte(kontaktanfrageDTO)
    }

    @GetMapping("/ladeUnternehmen")
    override fun ladeUnternehmen(): ResponseEntity<List<UnternehmenDTO>> {
        return unternehmenAdminService.ladeUnternehmen()
    }

    @PostMapping("/fuegeUnternehmenHinzu")
    override fun fuegeUnternehmenHinzu(unternehmenDTO: UnternehmenDTO): ResponseEntity<Unit> {
        return unternehmenAdminService.fuegeUnternehmenHinzu(unternehmenDTO)
    }

    @PostMapping("/aendereUnternehmen")
    override fun aendereUnternehmen(unternehmenDTO: UnternehmenDTO): ResponseEntity<Unit> {
        return unternehmenAdminService.aendereUnternehmen(unternehmenDTO)
    }

    @PostMapping("/loescheUnternehmen")
    override fun loescheUnternehmen(unternehmenDTO: UnternehmenDTO): ResponseEntity<Unit> {
        return unternehmenAdminService.loescheUnternehmen(unternehmenDTO)
    }
}

