package de.fewycoding.preisvergleich.model

data class PreisDTO(
        var ID: Int?,
        var datum: String?,
        var preis2700liter: String?,
        var preis4850liter: String?,
        var preis6400liter: String?,
        var plz: String?,
        var unternehmen: String?,
        var position: Int?
)