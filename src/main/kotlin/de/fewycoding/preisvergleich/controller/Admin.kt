package de.fewycoding.preisvergleich.controller



import de.fewycoding.preisvergleich.model.KontaktanfrageDTO
import de.fewycoding.preisvergleich.model.PreisDTO
import de.fewycoding.preisvergleich.model.UnternehmenDTO
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody

interface Admin {

    fun ladePreise(): ResponseEntity<List<PreisDTO>>

    fun fuegePreiseHinzu(@RequestBody preisDTO: PreisDTO): ResponseEntity<Unit>

    fun aenderePreise(@RequestBody preisDTO: PreisDTO): ResponseEntity<Unit>

    fun loeschePreise(@RequestBody preisDTO: PreisDTO): ResponseEntity<Unit>

    fun ladeKontakte(): ResponseEntity<List<KontaktanfrageDTO>>

    fun fuegeKontakteHinzu(@RequestBody kontaktanfrageDTO: KontaktanfrageDTO): ResponseEntity<Unit>

    fun aendereKontakte(@RequestBody kontaktanfrageDTO: KontaktanfrageDTO): ResponseEntity<Unit>

    fun loescheKontakte(@RequestBody kontaktanfrageDTO: KontaktanfrageDTO): ResponseEntity<Unit>

    fun ladeUnternehmen(): ResponseEntity<List<UnternehmenDTO>>

    fun fuegeUnternehmenHinzu(@RequestBody unternehmenDTO: UnternehmenDTO): ResponseEntity<Unit>

    fun aendereUnternehmen(@RequestBody unternehmenDTO: UnternehmenDTO): ResponseEntity<Unit>

    fun loescheUnternehmen(@RequestBody unternehmenDTO: UnternehmenDTO): ResponseEntity<Unit>
}