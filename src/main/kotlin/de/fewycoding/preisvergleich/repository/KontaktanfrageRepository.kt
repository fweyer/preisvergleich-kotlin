package de.fewycoding.preisvergleich.repository

import de.fewycoding.preisvergleich.entity.KontaktanfrageEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface KontaktanfrageRepository : JpaRepository<KontaktanfrageEntity, Int>