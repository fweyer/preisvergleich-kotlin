package de.fewycoding.preisvergleich

import de.fewycoding.preisvergleich.exception.KontaktanfrageException
import de.fewycoding.preisvergleich.model.KontaktanfrageDTO
import de.fewycoding.preisvergleich.repository.KontaktanfrageRepository
import de.fewycoding.preisvergleich.service.KontaktanfrageService
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import org.junit.After
import org.junit.Test
import org.junit.jupiter.api.assertThrows
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner

@SpringBootTest
@RunWith(SpringRunner::class)
class KontakanfrageServiceTest {

    @Autowired
    lateinit var kontaktanfrageService: KontaktanfrageService
    @Autowired
    lateinit var kontaktanfrageRepository: KontaktanfrageRepository

    @After
    fun deleteTestDataKontaktanfragen(){
        kontaktanfrageRepository.deleteAll()
    }

    @Test
    fun kontaktanfrageServiceTest_speichernOK(){
        val kontaktanfrageDTO = KontaktanfrageDTO(null,"TestGMBH","Musterstrasse","53120",
                        "Musterhausen","Testmann","Testmail","Testbetreff",
                        "Testnachrict",1)

        val response = kontaktanfrageService.konstaktAnfrageSpeichern(kontaktanfrageDTO)

       assertEquals(HttpStatus.OK,response.statusCode)
    }

    @Test
    fun kontaktanfrageServiceTest_speichernExceptionParameterFehlt(){
        val kontaktanfrageDTO = KontaktanfrageDTO(null,null,"Musterstrasse","53120",
                "Musterhausen","Testmann","Testmail","Testbetreff",
                "Testnachrict",1)

        val expectedException = assertThrows<KontaktanfrageException> {
            kontaktanfrageService.konstaktAnfrageSpeichern(kontaktanfrageDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Die eingegebenen Daten sind unvollständig!", expectedException.message)
    }
}