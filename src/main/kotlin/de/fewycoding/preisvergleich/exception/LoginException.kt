package de.fewycoding.preisvergleich.exception


class LoginException(message: String?) : PreisvergleichException(message)