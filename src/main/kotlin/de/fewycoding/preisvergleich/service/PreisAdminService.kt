package de.fewycoding.preisvergleich.service

import de.fewycoding.preisvergleich.extensions.toPreisUnternehmenEntity
import de.fewycoding.preisvergleich.model.PreisDTO
import de.fewycoding.preisvergleich.operations.PreisAdminOperation
import de.fewycoding.preisvergleich.repository.PreiseingabeUnternehmenRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.lang.RuntimeException

@Service
class PreisAdminService {

    @Autowired
    lateinit var preiseingabeUnternehmenRepository: PreiseingabeUnternehmenRepository

    fun ladePreise(): ResponseEntity<List<PreisDTO>> {
        val preisUnternehmenEntityListe = preiseingabeUnternehmenRepository.findAll()
        var preisliste = PreisAdminOperation.mappeZuPreisDTOs(preisUnternehmenEntityListe)
        preisliste = preisliste.sortedByDescending { it.ID }
        preisliste.forEachIndexed { index, dto -> dto.position = index + 1 }
        return ResponseEntity(preisliste,HttpStatus.OK)
    }

    fun fuegePreiseHinzu(preisDTO: PreisDTO): ResponseEntity<Unit> {
        PreisAdminOperation.validierePreisDTO(preisDTO)
        preiseingabeUnternehmenRepository.save(preisDTO.toPreisUnternehmenEntity())
        return ResponseEntity(HttpStatus.OK)
    }

    fun aenderePreise(preisDTO: PreisDTO): ResponseEntity<Unit> {
        PreisAdminOperation.validierePreisDTO(preisDTO)
        val oltEntity = preiseingabeUnternehmenRepository.getOne(preisDTO.ID!!)
        oltEntity.preis2700Liter = preisDTO.preis2700liter!!.toDouble()
        oltEntity.preis4850Liter = preisDTO.preis4850liter!!.toDouble()
        oltEntity.preis6400Liter = preisDTO.preis6400liter!!.toDouble()
        oltEntity.plz = preisDTO.plz.toString()
        preiseingabeUnternehmenRepository.save(oltEntity)
        return ResponseEntity(HttpStatus.OK)
    }

    fun loeschePreise(preisDTO: PreisDTO): ResponseEntity<Unit> {
        if (preisDTO.ID != null) {
            preiseingabeUnternehmenRepository.deleteById(preisDTO.ID!!)
            return ResponseEntity(HttpStatus.OK)
        }
        throw RuntimeException("Unternehmensdaten können nicht gelöscht werden!")
    }
}