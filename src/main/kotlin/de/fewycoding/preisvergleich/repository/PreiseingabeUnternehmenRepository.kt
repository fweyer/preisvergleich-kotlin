package de.fewycoding.preisvergleich.repository

import de.fewycoding.preisvergleich.entity.PreiseUnternehmenEntity
import de.fewycoding.preisvergleich.model.PreisDatenDTO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface PreiseingabeUnternehmenRepository : JpaRepository<PreiseUnternehmenEntity, Int> {

    @Query(value = "SELECT new de.fewycoding.preisvergleich.model.PreisDatenDTO(" +
            "p.id, p.preis2700Liter, p.datum, p.plz, " +
            "p.unternehmen.id, p.unternehmen.name, p.unternehmen.adresse, p.unternehmen.plz, p.unternehmen.ort) " +
            "FROM PreiseUnternehmenEntity p " +
            "WHERE p.plz = :var1 order by p.id desc",
            nativeQuery = false)
    fun holeAllePreiseZuPLZVon2700LiterTank(@Param("var1") plz: String?): List<PreisDatenDTO>


    @Query(value = "SELECT new de.fewycoding.preisvergleich.model.PreisDatenDTO(" +
            "p.id, p.preis4850Liter, p.datum, p.plz, " +
            "p.unternehmen.id, p.unternehmen.name, p.unternehmen.adresse, p.unternehmen.plz, p.unternehmen.ort) " +
            "FROM PreiseUnternehmenEntity p " +
            "WHERE p.plz = :var1 order by p.id desc",
            nativeQuery = false)
    fun holeAllePreiseZuPLZVon4850LiterTank(@Param("var1") plz: String?): List<PreisDatenDTO>


    @Query(value = "SELECT new de.fewycoding.preisvergleich.model.PreisDatenDTO( " +
            "p.id, p.preis6400Liter, p.datum, p.plz, " +
            "p.unternehmen.id, p.unternehmen.name, p.unternehmen.adresse, p.unternehmen.plz, p.unternehmen.ort) " +
            "FROM PreiseUnternehmenEntity p " +
            "WHERE p.plz = :var1 order by p.id desc",
            nativeQuery = false)
    fun holeAllePreiseZuPLZVon6400LiterTank(@Param("var1") plz: String?): List<PreisDatenDTO>
}
