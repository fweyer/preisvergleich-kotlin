package de.fewycoding.preisvergleich.controller

import de.fewycoding.preisvergleich.model.KontaktanfrageDTO
import de.fewycoding.preisvergleich.service.KontaktanfrageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/kontakt")
class KontaktanfrageController : Kontaktanfrage {

    @Autowired
    lateinit var kontaktanfrageService: KontaktanfrageService


    @PostMapping("/kontaktUnternehmen")
    override fun konstaktAnfrageSpeichern(kontaktAnfrageDTO: KontaktanfrageDTO?): ResponseEntity<String> {
        return kontaktanfrageService.konstaktAnfrageSpeichern(kontaktAnfrageDTO)
    }

}