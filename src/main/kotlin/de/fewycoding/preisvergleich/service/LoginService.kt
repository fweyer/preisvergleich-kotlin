package de.fewycoding.preisvergleich.service

import de.fewycoding.preisvergleich.exception.LoginException
import de.fewycoding.preisvergleich.model.LoginDTO
import de.fewycoding.preisvergleich.operations.LoginOperations
import de.fewycoding.preisvergleich.repository.UnternehmensRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class LoginService {

    @Autowired
    lateinit var unternehemensRepository: UnternehmensRepository

    fun login(loginDTO: LoginDTO?): ResponseEntity<String> {
        val ergebnis: String = LoginOperations.loginCheck(loginDTO, unternehemensRepository.findAll())
        if ("fehlgeschlagen" == ergebnis) throw LoginException("Der Benutzername oder das Passwort stimmen nicht")
        return ResponseEntity(ergebnis,HttpStatus.OK)
    }
}