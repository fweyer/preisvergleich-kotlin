package de.fewycoding.preisvergleich.controller

import de.fewycoding.preisvergleich.model.LoginDTO
import de.fewycoding.preisvergleich.service.LoginService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/login")
class LoginController : Login {

    @Autowired
    lateinit var loginService: LoginService

    @CrossOrigin("http://localhost:3000")
    @PostMapping("/logincheck")
    override fun login(loginDTO: LoginDTO?): ResponseEntity<String> {
        return loginService.login(loginDTO)
    }

}