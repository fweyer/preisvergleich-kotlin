package de.fewycoding.preisvergleich.extensions

import PreisAnzeigeDTO
import de.fewycoding.preisvergleich.model.PreisDatenDTO

fun PreisDatenDTO.toPreisAnzeigeDTO() = PreisAnzeigeDTO(
        id = id as Int,
        name = unternehmenName,
        adresse = unternehmenAdresse,
        plz = unternehmenPlz,
        ort = unternehmenOrt,
        preis = preis.toString()
)