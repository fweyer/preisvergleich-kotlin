package de.fewycoding.preisvergleich.exception


class PLZExistiertNichtException(message: String?) : PreisvergleichException(message)