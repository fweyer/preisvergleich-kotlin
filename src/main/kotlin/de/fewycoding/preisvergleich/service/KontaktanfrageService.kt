package de.fewycoding.preisvergleich.service

import de.fewycoding.preisvergleich.controller.Kontaktanfrage
import de.fewycoding.preisvergleich.extensions.toKontaktAnfrageEntity
import de.fewycoding.preisvergleich.model.KontaktanfrageDTO
import de.fewycoding.preisvergleich.operations.KontaktAnfrageOperations
import de.fewycoding.preisvergleich.repository.KontaktanfrageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class KontaktanfrageService : Kontaktanfrage {

    @Autowired
    lateinit var kontaktanfrageRepository: KontaktanfrageRepository

    override fun konstaktAnfrageSpeichern(kontaktAnfrageDTO: KontaktanfrageDTO?): ResponseEntity<String> {
        KontaktAnfrageOperations.validiereKontaktanfrageDTO(kontaktAnfrageDTO)
        kontaktanfrageRepository.save(kontaktAnfrageDTO!!.toKontaktAnfrageEntity())
        return ResponseEntity("erfolgreich",HttpStatus.OK)
    }
}