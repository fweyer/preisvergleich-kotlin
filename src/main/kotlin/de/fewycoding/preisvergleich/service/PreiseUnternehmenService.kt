package de.fewycoding.preisvergleich.service

import PreisAnzeigeDTO
import de.fewycoding.preisvergleich.controller.PreiseUnternehmen
import de.fewycoding.preisvergleich.exception.PreiseingabeException
import de.fewycoding.preisvergleich.extensions.toPreisUnterhnehmenEntity
import de.fewycoding.preisvergleich.model.AnfrageDTO
import de.fewycoding.preisvergleich.model.PreisDatenDTO
import de.fewycoding.preisvergleich.model.PreiseingabeUnternehmenDTO
import de.fewycoding.preisvergleich.operations.PreiseUnternehmenOperations
import de.fewycoding.preisvergleich.repository.PreiseingabeUnternehmenRepository
import de.fewycoding.preisvergleich.repository.UnternehmensRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class PreiseUnternehmenService : PreiseUnternehmen {

    @Autowired
    lateinit var unternehmenRepository: UnternehmensRepository

    @Autowired
    lateinit var preiseingabeUnternehmenRepository: PreiseingabeUnternehmenRepository

    override fun anfragePreise(anfrageDTO: AnfrageDTO?): ResponseEntity<List<PreisAnzeigeDTO>> {
        PreiseUnternehmenOperations.validateAnfrageDTO(anfrageDTO)
        var preiseUnternehmensObjekte = bestimmePreiseAnhandTankUndPLZ(anfrageDTO!!)
        if (preiseUnternehmensObjekte.isEmpty()) throw PreiseingabeException("Keine Preise verfügbar")
        val anzahlUnternehmen = unternehmenRepository.findAll().size
        preiseUnternehmensObjekte = PreiseUnternehmenOperations.bestimmeLetztenPreisVonUnternehmen(preiseUnternehmensObjekte, anzahlUnternehmen)
        var preisliste = PreiseUnternehmenOperations.mappePreisObjekteZuPreisdatenDTO(preiseUnternehmensObjekte)
        preisliste = preisliste.sortedBy { it.preis }
        preisliste.forEach { PreiseUnternehmenOperations.formatierePreise(it) }
        return ResponseEntity(preisliste,HttpStatus.OK)
    }

    override fun preiseingabeLogin(preiseingabeUnternehmenDTO: PreiseingabeUnternehmenDTO?): ResponseEntity<String> {
        PreiseUnternehmenOperations.validierePreiseingabe(preiseingabeUnternehmenDTO)
        val preis = preiseingabeUnternehmenDTO!!.toPreisUnterhnehmenEntity()
        val unternehmenid = PreiseUnternehmenOperations.bestimmeUnternehmenZuBenutzer(unternehmenRepository.findAll(), preiseingabeUnternehmenDTO)
        preis.unternehmen = unternehmenid ?: throw PreiseingabeException("Der Benutzer exsistiert nicht")
        preiseingabeUnternehmenRepository.save(preis)
        return ResponseEntity("erfolgreich",HttpStatus.OK)
    }

    private fun bestimmePreiseAnhandTankUndPLZ(anfrageDTO: AnfrageDTO): List<PreisDatenDTO> {
        return when (anfrageDTO.behaelter) {
            "preis2700liter" -> preiseingabeUnternehmenRepository.holeAllePreiseZuPLZVon2700LiterTank(anfrageDTO.plz)
            "preis4850liter" -> preiseingabeUnternehmenRepository.holeAllePreiseZuPLZVon4850LiterTank(anfrageDTO.plz)
            "preis6400liter" -> preiseingabeUnternehmenRepository.holeAllePreiseZuPLZVon6400LiterTank(anfrageDTO.plz)
            else -> emptyList()
        }
    }
}