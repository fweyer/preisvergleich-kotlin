package de.fewycoding.preisvergleich

import de.fewycoding.preisvergleich.exampledata.ExampleData
import de.fewycoding.preisvergleich.repository.UnternehmensRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import javax.annotation.PostConstruct


@SpringBootApplication
open class PreisvergleichKotlinApplication {

	@Autowired
	lateinit var unternehemensRepository: UnternehmensRepository

	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
			runApplication<PreisvergleichKotlinApplication>(*args)
		}
	}
	//befüllt die Datenbank mit Testdaten falls die noch nicht vorhanden sind.
	@PostConstruct
	fun init() {
		println("Init wurde aufgerufen ")
		ExampleData.initExampleData(unternehemensRepository)
		println("Init wurde beendet ")
	}
}