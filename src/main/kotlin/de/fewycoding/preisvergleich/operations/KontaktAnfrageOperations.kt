package de.fewycoding.preisvergleich.operations

import de.fewycoding.preisvergleich.exception.KontaktanfrageException
import de.fewycoding.preisvergleich.model.KontaktanfrageDTO


class KontaktAnfrageOperations {

    companion object {

        fun validiereKontaktanfrageDTO(kontaktanfrageDTO: KontaktanfrageDTO?) {
            if (kontaktAnfrageHatNullEintraegeOderLeer(kontaktanfrageDTO)) {
                throw KontaktanfrageException("Die eingegebenen Daten sind unvollständig!")
            }
        }

        private fun kontaktAnfrageHatNullEintraegeOderLeer(kontaktanfrage: KontaktanfrageDTO?): Boolean {
            return  kontaktanfrage?.firmennamen.isNullOrEmpty() ||
                    kontaktanfrage?.firmenadresse.isNullOrEmpty() ||
                    kontaktanfrage?.plz.isNullOrEmpty() ||
                    kontaktanfrage?.ort.isNullOrEmpty() ||
                    kontaktanfrage?.kontaktperson.isNullOrEmpty() ||
                    kontaktanfrage?.betreff.isNullOrEmpty() ||
                    kontaktanfrage?.eMail.isNullOrEmpty() ||
                    kontaktanfrage?.nachricht.isNullOrEmpty()
        }
    }
}