package de.fewycoding.preisvergleich.exception

open class PreisvergleichException(message: String?) : RuntimeException(message)