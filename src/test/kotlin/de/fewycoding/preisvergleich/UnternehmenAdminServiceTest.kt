package de.fewycoding.preisvergleich

import de.fewycoding.preisvergleich.entity.UnternehmenEntity
import de.fewycoding.preisvergleich.model.UnternehmenDTO
import de.fewycoding.preisvergleich.repository.UnternehmensRepository
import de.fewycoding.preisvergleich.service.UnternehmenAdminService
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.assertThrows
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import java.lang.RuntimeException

@SpringBootTest
@RunWith(SpringRunner::class)
class UnternehmenAdminServiceTest {

    @Autowired
    lateinit var unternehmensRepository: UnternehmensRepository

    @Autowired
    lateinit var unternehmenAdminService: UnternehmenAdminService

    @Before
    fun initTestData() {
        val unternehmenOne = UnternehmenEntity(1, "Meinflüssiggas", "Irgendwo 169", "26212", "Oldenburg", "admin", "admin".hashCode())
        val unternehmenTwo = UnternehmenEntity(2, "Flüssiggas456", "Musterstraße 70", "98092", "Erfurt", "login", "login".hashCode())
        val unternehmenThree = UnternehmenEntity(3, "Beispielunternehmen", "ABC str.90", "50000", "Köln", "bespiel", "beispiel".hashCode())

        unternehmensRepository.save(unternehmenOne)
        unternehmensRepository.save(unternehmenTwo)
        unternehmensRepository.save(unternehmenThree)
    }

    @After
    fun deleteTestDataKontaktanfragen() {
        unternehmensRepository.deleteAll()
    }

    @Test
    fun unternehmenAdminServiceTest_ladeUnternehmenTestOK() {
        val response = unternehmenAdminService.ladeUnternehmen()

        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(3, response.body?.size)
        assertEquals("98092", response.body?.get(1)?.plz)
    }

    @Test
    fun unternehmenAdminServiceTest_fuegeUnternehmenHinzuOK() {
        val unternehmenDTO = UnternehmenDTO(4, "Testunternehmen", "DEF str.100", "60000", "Köln", "test", "test".hashCode())

        val response = unternehmenAdminService.fuegeUnternehmenHinzu(unternehmenDTO)

        assertEquals(HttpStatus.OK, response.statusCode)
    }

    @Test
    fun unternehmenAdminServiceTest_fuegeUnternehmenHinzuParameterFehltException() {
        val unternehmenDTO = UnternehmenDTO(4, "Testunternehmen", "DEF str.100", "60000", null, "test", "test".hashCode())

        val expectedException = assertThrows<RuntimeException> {
            unternehmenAdminService.fuegeUnternehmenHinzu(unternehmenDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Unternehmensdaten sind unvollständig", expectedException.message)
    }

    @Test
    fun unternehmenAdminServiceTest_aendereUnternehmenOK() {
        val letztesElement = unternehmensRepository.findAll().size
        val unternehmenDTO = UnternehmenDTO(letztesElement, "Test", "Test", "50102", "Test", "Test", 4)

        val response = unternehmenAdminService.aendereUnternehmen(unternehmenDTO)

        assertEquals(HttpStatus.OK, response.statusCode)
    }

    @Test
    fun unternehmenAdminServiceTest_aendereUnternehmenParameterFehltException() {
        val letztesElement = unternehmensRepository.findAll().size
        val unternehmenDTO = UnternehmenDTO(letztesElement, null, "Test", "50102", "Test", "Test", 4)

        val expectedException = assertThrows<RuntimeException> {
            unternehmenAdminService.aendereUnternehmen(unternehmenDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Unternehmensdaten sind unvollständig", expectedException.message)
    }

    @Test
    fun unternehmenAdminServiceTest_loescheUnternehmenOK() {
        val unternehmenListe = unternehmensRepository.findAll()
        val letztesElement = unternehmenListe.size
        val unternehmenDTO = UnternehmenDTO(letztesElement, "Beispielunternehmen", "ABC str.90", "50000", "Köln", "bespiel", 4)

        val response = unternehmenAdminService.loescheUnternehmen(unternehmenDTO)

        assertEquals(HttpStatus.OK, response.statusCode)
    }

    @Test
    fun unternehmenAdminServiceTest_loescheUnternehmenException() {
        val unternehmenDTO = UnternehmenDTO(null, "Beispielunternehmen", "ABC str.90", "50000", "Köln", "bespiel", 4)

        val expectedException = assertThrows<RuntimeException> {
            unternehmenAdminService.loescheUnternehmen(unternehmenDTO)
        }

        assertNotNull(expectedException)
        assertEquals("Diese Unternehmensdaten können nicht gelöscht werden", expectedException.message)
    }
}