package de.fewycoding.preisvergleich.operations

import de.fewycoding.preisvergleich.entity.PreiseUnternehmenEntity
import de.fewycoding.preisvergleich.exception.PreiseingabeException
import de.fewycoding.preisvergleich.extensions.toPreisDTO
import de.fewycoding.preisvergleich.model.PreisDTO


class PreisAdminOperation {

    companion object {
        fun validierePreisDTO(preisDTO: PreisDTO) {
            if (preisDTOHasNullEntries(preisDTO))
                throw PreiseingabeException("Der Eingegebene Preis ist leer oder enthält unvollständige Werte")
        }

        fun mappeZuPreisDTOs(preisUnternehmenEntityListe: List<PreiseUnternehmenEntity>): List<PreisDTO> {
            val preisDTOliste = mutableListOf<PreisDTO>()
            preisUnternehmenEntityListe.forEach { preisDTOliste.add(it.toPreisDTO()) }
            return preisDTOliste
        }

        private fun preisDTOHasNullEntries(preisDTO: PreisDTO): Boolean {
            return  preisDTO.preis2700liter.isNullOrEmpty() ||
                    preisDTO.preis4850liter.isNullOrEmpty() ||
                    preisDTO.preis6400liter.isNullOrEmpty() ||
                    preisDTO.datum == null ||
                    preisDTO.unternehmen == null
        }
    }
}