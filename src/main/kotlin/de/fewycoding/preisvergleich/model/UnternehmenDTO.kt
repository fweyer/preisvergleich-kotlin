package de.fewycoding.preisvergleich.model

data class UnternehmenDTO(
        var ID: Int?,
        var name: String?,
        var adresse: String?,
        var plz: String?,
        var ort: String?,
        var benutzername: String?,
        var position: Int?
)