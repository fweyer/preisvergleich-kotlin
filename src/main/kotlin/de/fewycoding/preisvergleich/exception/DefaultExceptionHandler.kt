package de.fewycoding.preisvergleich.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class DefaultExceptionHandler {

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    fun handleException(exc: PreisvergleichException): String {
        return exc.message ?: "Es ist ein Fehler aufgetreten"
    }
}