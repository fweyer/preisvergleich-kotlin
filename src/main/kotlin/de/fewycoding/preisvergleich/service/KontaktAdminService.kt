package de.fewycoding.preisvergleich.service

import de.fewycoding.preisvergleich.exception.KontaktanfrageException
import de.fewycoding.preisvergleich.extensions.toKontaktAnfrageDTO
import de.fewycoding.preisvergleich.extensions.toKontaktAnfrageEntity
import de.fewycoding.preisvergleich.model.KontaktanfrageDTO
import de.fewycoding.preisvergleich.operations.KontaktAnfrageOperations
import de.fewycoding.preisvergleich.repository.KontaktanfrageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class KontaktAdminService {

    @Autowired
    lateinit var kontaktanfrageRepository: KontaktanfrageRepository

    fun ladeKontakte(): ResponseEntity<List<KontaktanfrageDTO>> {
        val listeKontakte = mutableListOf<KontaktanfrageDTO>()
        kontaktanfrageRepository.findAll().forEach { listeKontakte.add(it.toKontaktAnfrageDTO()) }
        listeKontakte.sortByDescending { it.ID }
        listeKontakte.forEachIndexed { index, dto -> dto.position = index + 1 }
        return ResponseEntity(listeKontakte, HttpStatus.OK)
    }

    fun fuegeKontakteHinzu(kontaktanfrageDTO: KontaktanfrageDTO): ResponseEntity<Unit> {
        KontaktAnfrageOperations.validiereKontaktanfrageDTO(kontaktanfrageDTO)
        val kontaktanfrageEntity = kontaktanfrageDTO.toKontaktAnfrageEntity()
        kontaktanfrageRepository.save(kontaktanfrageEntity)
        return ResponseEntity(HttpStatus.OK)
    }

    fun aendereKontakte(kontaktanfrageDTO: KontaktanfrageDTO): ResponseEntity<Unit> {
        if (kontaktanfrageDTO.ID != null) {
            KontaktAnfrageOperations.validiereKontaktanfrageDTO(kontaktanfrageDTO)
            val oltEntity = kontaktanfrageRepository.getOne(kontaktanfrageDTO.ID!!)
            oltEntity.firmennamen = kontaktanfrageDTO.firmennamen
            oltEntity.firmenadresse = kontaktanfrageDTO.firmenadresse
            oltEntity.ort = kontaktanfrageDTO.ort
            oltEntity.plz = kontaktanfrageDTO.plz
            oltEntity.kontaktperson = kontaktanfrageDTO.kontaktperson
            oltEntity.nachricht = kontaktanfrageDTO.nachricht
            oltEntity.emailAdresse = kontaktanfrageDTO.eMail
            oltEntity.betreff = kontaktanfrageDTO.betreff
            kontaktanfrageRepository.save(oltEntity)
            return ResponseEntity(HttpStatus.OK)
        }
        throw KontaktanfrageException("Diese Kontaktdaten können nicht aktualisiert werden")
    }

    fun loescheKontakte(kontaktanfrageDTO: KontaktanfrageDTO): ResponseEntity<Unit> {
        if (kontaktanfrageDTO.ID != null) {
            kontaktanfrageRepository.deleteById(kontaktanfrageDTO.ID!!)
            return ResponseEntity(HttpStatus.OK)
        }
        throw KontaktanfrageException("Diese Kontaktdaten können nicht gelöscht werden")
    }
}