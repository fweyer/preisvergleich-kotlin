package de.fewycoding.preisvergleich.operations

import de.fewycoding.preisvergleich.model.UnternehmenDTO
import java.lang.RuntimeException

class UnternehmensOperations {
    companion object {

        fun validiereUnternehmenDTO(unternehmenDTO: UnternehmenDTO) {
            if (unternehmensDToHasNullEntrys(unternehmenDTO))
                throw RuntimeException("Unternehmensdaten sind unvollständig")
        }

        private fun unternehmensDToHasNullEntrys(unternehmenDTO: UnternehmenDTO): Boolean {
            return  unternehmenDTO.name.isNullOrEmpty() ||
                    unternehmenDTO.adresse.isNullOrEmpty() ||
                    unternehmenDTO.plz.isNullOrEmpty() ||
                    unternehmenDTO.ort.isNullOrEmpty()
        }
    }
}