package de.fewycoding.preisvergleich.exception


class TankException(message: String?) : PreisvergleichException(message)