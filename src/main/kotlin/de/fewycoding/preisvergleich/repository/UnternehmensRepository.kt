package de.fewycoding.preisvergleich.repository

import de.fewycoding.preisvergleich.entity.UnternehmenEntity
import org.springframework.data.jpa.repository.JpaRepository

interface UnternehmensRepository : JpaRepository<UnternehmenEntity,Int>