package de.fewycoding.preisvergleich.extensions

import de.fewycoding.preisvergleich.entity.KontaktanfrageEntity
import de.fewycoding.preisvergleich.entity.PreiseUnternehmenEntity
import de.fewycoding.preisvergleich.entity.UnternehmenEntity
import de.fewycoding.preisvergleich.model.KontaktanfrageDTO
import de.fewycoding.preisvergleich.model.PreisDTO
import de.fewycoding.preisvergleich.model.UnternehmenDTO


fun KontaktanfrageEntity.toKontaktAnfrageDTO() = KontaktanfrageDTO(
        ID = id,
        firmennamen = firmennamen,
        firmenadresse = firmenadresse,
        plz = plz,
        ort = ort,
        kontaktperson = kontaktperson,
        betreff = betreff,
        eMail = emailAdresse,
        nachricht = nachricht,
        position = 0
)

fun PreiseUnternehmenEntity.toPreisDTO() = PreisDTO(
        ID = id,
        datum = datum.toString(),
        preis2700liter = preis2700Liter.toString(),
        preis4850liter = preis4850Liter.toString(),
        preis6400liter = preis6400Liter.toString(),
        plz = plz,
        unternehmen = unternehmen?.name,
        position = 0
)

fun UnternehmenEntity.toUnternehmenDTO() = UnternehmenDTO(
        ID = id,
        name = name,
        adresse = adresse,
        plz = plz,
        ort = ort,
        benutzername = benutzername,
        position = 0
)

