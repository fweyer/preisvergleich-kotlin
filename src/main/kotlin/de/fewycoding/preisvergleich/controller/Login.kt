package de.fewycoding.preisvergleich.controller


import de.fewycoding.preisvergleich.model.LoginDTO
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody

interface Login {

    fun login(@RequestBody loginDTO: LoginDTO?): ResponseEntity<String>
}