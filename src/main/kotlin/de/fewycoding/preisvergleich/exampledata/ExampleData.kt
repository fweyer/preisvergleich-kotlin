package de.fewycoding.preisvergleich.exampledata

import de.fewycoding.preisvergleich.entity.PreiseUnternehmenEntity
import de.fewycoding.preisvergleich.entity.UnternehmenEntity
import de.fewycoding.preisvergleich.repository.UnternehmensRepository
import java.time.LocalDate

class ExampleData {
    companion object{

        fun initExampleData(unternehemensRepository: UnternehmensRepository){
            val unternehmen = arrayOf(
                    arrayOf("Meinflüssiggas", "Irgendwo 169", "26212", "Oldenburg", "admin", "admin"),
                    arrayOf("Flüssiggas456", "Musterstraße 70", "98092 ", "Erfurt", "login", "login"),
                    arrayOf("Beispielunternehmen", "ABC str.90", "50000", "Köln", "bespiel", "beispiel"),
                    arrayOf("Testunternehmen", "DEF str.100", "60000", "Köln", "test", "test"))

            val preiseMeinFluessiggas = mutableListOf(
                    PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(1)),
                    PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(1)),
                    PreiseUnternehmenEntity(null,40.00, 37.00, 36.00, LocalDate.of(2020,2,26),"51570", UnternehmenEntity(1)),
                    PreiseUnternehmenEntity(null,40.00, 37.00 , 36.00 , LocalDate.of(2020,2,26), "51570", UnternehmenEntity(1)),
                    PreiseUnternehmenEntity(null, 42.00, 41.00, 39.11, LocalDate.of(2020,2,27), "52152", UnternehmenEntity(1))

            )
            val preiseFluessiggas456 = mutableListOf(
                    PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(2)),
                    PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(2)),
                    PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,26), "52152", UnternehmenEntity(2)),
                    PreiseUnternehmenEntity(null,40.50, 37.50 , 36.00, LocalDate.of(2020,2,26), "51570", UnternehmenEntity(2)),
                    PreiseUnternehmenEntity(null,40.50 , 37.50 , 36.00, LocalDate.of(2020,2,27),"51570", UnternehmenEntity(2)),
                    PreiseUnternehmenEntity(null,42.00 , 41.00, 39.00, LocalDate.of(2020,2,27), "52152", UnternehmenEntity(2))

            )
            val preiseBeispielunternehmen = mutableListOf(
                    PreiseUnternehmenEntity(null,38.30, 37.30, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(3)),
                    PreiseUnternehmenEntity(null,38.00, 37.00, 36.00, LocalDate.of(2020,2,25), "51570", UnternehmenEntity(3)),
                    PreiseUnternehmenEntity(null,40.30, 37.30, 36.00, LocalDate.of(2020,2,26), "51570", UnternehmenEntity(3)),
                    PreiseUnternehmenEntity(null,38.00 , 37.00, 36.00, LocalDate.of(2020,2,26), "52152", UnternehmenEntity(3)),
                    PreiseUnternehmenEntity(null,40.30, 37.30, 36.00, LocalDate.of(2020,2,27), "51570", UnternehmenEntity(3)),
                    PreiseUnternehmenEntity(null,42.00, 41.00, 39.00, LocalDate.of(2020,2,27), "52152", UnternehmenEntity(3))
            )

            val preiseTestunternehmen = mutableListOf(
                    PreiseUnternehmenEntity(null,41.00, 39.00, 35.00, LocalDate.of(2020,2,26), "51570", UnternehmenEntity(4)),
                    PreiseUnternehmenEntity(null,41.00, 39.00, 35.00, LocalDate.of(2020,2,27), "51570", UnternehmenEntity(4))
            )

            val preisliste = mutableListOf<MutableList<PreiseUnternehmenEntity>>()
            preisliste.add(mutableListOf())
            preisliste.add(preiseMeinFluessiggas)
            preisliste.add(preiseFluessiggas456)
            preisliste.add(preiseBeispielunternehmen)
            preisliste.add(preiseTestunternehmen)

            if (unternehemensRepository.findAll().isEmpty()) {
                var i = 1
                for (einezelnesUnternehmen in unternehmen) {
                    val entity = UnternehmenEntity(
                            id = i,
                            name = einezelnesUnternehmen[0],
                            adresse = einezelnesUnternehmen[1],
                            plz = einezelnesUnternehmen[2],
                            ort = einezelnesUnternehmen[3],
                            benutzername = einezelnesUnternehmen[4],
                            passwort = einezelnesUnternehmen[5].hashCode(),
                            preise = preisliste[i]
                    )
                    unternehemensRepository.save(entity)
                    i++
                }
            }
        }
    }
}