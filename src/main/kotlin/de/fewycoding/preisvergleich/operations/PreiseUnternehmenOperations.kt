package de.fewycoding.preisvergleich.operations

import PreisAnzeigeDTO
import de.fewycoding.preisvergleich.entity.UnternehmenEntity
import de.fewycoding.preisvergleich.exception.PLZExistiertNichtException
import de.fewycoding.preisvergleich.exception.PreiseingabeException
import de.fewycoding.preisvergleich.exception.TankException
import de.fewycoding.preisvergleich.extensions.toPreisAnzeigeDTO
import de.fewycoding.preisvergleich.model.AnfrageDTO
import de.fewycoding.preisvergleich.model.PreisDatenDTO
import de.fewycoding.preisvergleich.model.PreiseingabeUnternehmenDTO

import java.lang.RuntimeException


class PreiseUnternehmenOperations {

    companion object {
        fun mappePreisObjekteZuPreisdatenDTO(preisObjectList: List<PreisDatenDTO>): List<PreisAnzeigeDTO> {
            val preisAnzeigeDTOList = mutableListOf<PreisAnzeigeDTO>()
            preisObjectList.forEach {
                preisAnzeigeDTOList.add(it.toPreisAnzeigeDTO())
            }
            return preisAnzeigeDTOList
        }

        fun validateAnfrageDTO(anfrageDTO: AnfrageDTO?) {
            if (anfrageDTO == null) return throw RuntimeException("Die Anfrage ist Leer")
            if (anfrageDTO.behaelter.isNullOrEmpty()) throw TankException("Bitte geben Sie die Tankgröße an")
            if (anfrageDTO.plz.isNullOrEmpty()) throw PLZExistiertNichtException("Die angegebene PLZ existiert nicht")
        }

        fun bestimmeLetztenPreisVonUnternehmen(preiseUnternehmenObjekte: List<PreisDatenDTO>, anzahlUnternehmen: Int)
                : List<PreisDatenDTO> {
            val letztePreiseUnternehmensObjekteVonUnternehmen = mutableListOf<PreisDatenDTO>()
            for (i in anzahlUnternehmen downTo 1) {
                for (preisdatenDTO in preiseUnternehmenObjekte) {
                    if (preisdatenDTO.unternehmenID as Int == i) {
                        letztePreiseUnternehmensObjekteVonUnternehmen.add(preisdatenDTO)
                        break
                    }
                }
            }
            return letztePreiseUnternehmensObjekteVonUnternehmen
        }

        fun validierePreiseingabe(preiseingabeUnternehmenDTO: PreiseingabeUnternehmenDTO?) {
            if (preiseingabeUnternehmenDTOHasNullEntrys(preiseingabeUnternehmenDTO))
                throw PreiseingabeException("Die Preiseingaben sind unvollständig")
        }

        private fun preiseingabeUnternehmenDTOHasNullEntrys(preiseingabeUnternehmenDTO: PreiseingabeUnternehmenDTO?): Boolean {
            return  preiseingabeUnternehmenDTO?.preis2700Liter.isNullOrEmpty() ||
                    preiseingabeUnternehmenDTO?.preis4850Liter.isNullOrEmpty() ||
                    preiseingabeUnternehmenDTO?.preis6400Liter.isNullOrEmpty() ||
                    preiseingabeUnternehmenDTO?.postleitzahl.isNullOrEmpty()
        }

        fun bestimmeUnternehmenZuBenutzer(unternehmen: List<UnternehmenEntity>, preiseingabeUnternehmenDTO: PreiseingabeUnternehmenDTO)
                : UnternehmenEntity? {
            unternehmen.forEach {
                if (it.benutzername == preiseingabeUnternehmenDTO.benutzername) return it
            }
            return null
        }

        fun formatierePreise(preis: PreisAnzeigeDTO) {
           var newPreis =  preis.preis?.replace(".", ",")
           if (newPreis?.length!! == 4){
               newPreis += "0 €"
               preis.preis = newPreis
           }
           else{
               newPreis += " €"
               preis.preis = newPreis
           }
        }
    }
}