package de.fewycoding.preisvergleich.controller

import PreisAnzeigeDTO
import de.fewycoding.preisvergleich.model.AnfrageDTO
import de.fewycoding.preisvergleich.model.PreiseingabeUnternehmenDTO
import de.fewycoding.preisvergleich.service.PreiseUnternehmenService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin("http://localhost:3000")
@RequestMapping("/preis")
@RestController
class PreiseUnternehmenController : PreiseUnternehmen {

    @Autowired
    lateinit var preiseUnternehmenService: PreiseUnternehmenService

    @PostMapping("/anfrage")
    override fun anfragePreise(anfrageDTO: AnfrageDTO?): ResponseEntity<List<PreisAnzeigeDTO>> {
        return preiseUnternehmenService.anfragePreise(anfrageDTO)
    }

    @PostMapping("/preiseingabe")
    override fun preiseingabeLogin(preiseingabeUnternehmenDTO: PreiseingabeUnternehmenDTO?): ResponseEntity<String> {
        return preiseUnternehmenService.preiseingabeLogin(preiseingabeUnternehmenDTO)
    }
}