#!/bin/bash
echo "Starte deployment zum Raspberry PI"
echo "Beginne mit dem Bau und dem Test des Artefaktes"
sleep 2
mvn clean package
if [ $? -ne 0 ]
	then 
	echo "Das bilden des Artefaktes ist fehlgeschlagen" 
	echo "Script beendet"
	exit
fi
echo "Das Artefakt konnte erfolgreich erstellt werden"
sleep 2
rm $HOME/raspi-deploy-verzeichnis/*.jar  
cp $PWD/target/*.jar $HOME/raspi-deploy-verzeichnis
if [ $? -ne 0 ]
	then 
	echo "Die jar Datei konnte nicht in das Deploy Verzeichnis verschoben werden"
	exit
fi
echo "Jar Datei erfolgreich in das Deploy Verzeichnis verschoben"
sleep 2
cd $HOME/raspi-deploy-verzeichnis
ssh -t -i id_rsa pi@raspberrypi "rm /home/pi/Desktop/deployments/*.jar"
if [ $? -eq 0 ]
	then 
	echo "Alte jar Datein im Raspberrypi entfernt"
	sleep 2
fi
echo "Beginne mit der Installation auf dem Raspberry"	
scp -r -i id_rsa $HOME/raspi-deploy-verzeichnis/*.jar pi@raspberrypi:/home/pi/Desktop/deployments
if [ $? -eq 0 ]
	then 
	echo "Neue Jar Datei erfolgreich auf den Raspberry verschoben"
fi
echo "Starte Jar-Datei auf dem Rasberrypi"
sleep 2
ssh -t -i id_rsa pi@raspberrypi "java -jar /home/pi/Desktop/deployments/*.jar"
exit

