package de.fewycoding.preisvergleich.model

data class AnfrageDTO(
        var behaelter: String?,
        var plz: String?
)